using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {
    public GUIStyle buttonstyle;
    public GUIStyle deathstyle;
    public GUIStyle statstyle;
    bool highscore;

    int score;

    void Start()
    {
        PlayerPrefs.DeleteKey("Version");
        score = Mathf.RoundToInt((Stats.KillCount * 3 + Stats.GlassCount) / 2) * GameWorld.enemycount;
        highscore = Globalstats.SetHighScore(score);
        Debug.Log(highscore);
    }

    void Update()
    {        
        buttonstyle.fontSize = Screen.height / 15;
        deathstyle.fontSize = Screen.height / 5;
    }

    void OnGUI()
    {
        
        //GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Health.GameOverPic);
        GUI.Label(new Rect(Screen.width / 4, Screen.height / 10, Screen.width / 2, Screen.height / 4), "You died!", deathstyle);
        GUI.Box(new Rect(Screen.width / 4, Screen.height / 2, Screen.width / 2, Screen.height / 4 - 5), "You killed " + Stats.KillCount.ToString() + " evil shapes.\n You shattered " + Stats.GlassCount.ToString() + " pieces of glass.\n You reached level " + GameWorld.enemycount +"\n This earns you " + score + " points." + (highscore ? "\nThis is your new high score." : "\nYou can do better!"), statstyle);
        if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 2 + Screen.height / 4, Screen.width / 2, Screen.height / 8), "Back to main menu", buttonstyle))
        {
            GameWorld.Reset();
            Application.LoadLevel("Main menu");
        }
    }
}
