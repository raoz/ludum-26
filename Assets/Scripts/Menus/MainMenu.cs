using UnityEngine;
using System.Collections;

public enum Menu
{
    Mainmenu,
    Play,
    SaveWillBeLost,
    Stats
}

public class MainMenu : MonoBehaviour {
    public GUIStyle buttonstyle;
    public GUIStyle titlestyle;
    public GUIStyle warningstyle;
    public GUIStyle statstyle;
    public GUIStyle attribution;
    public Menu menu;
    
    void Update()
    {
        buttonstyle.fontSize = Screen.height / 12;
        titlestyle.fontSize = Screen.height / 5;
    }
    void OnGUI()
    {
        GUI.Label(new Rect(Screen.width / 8, Screen.height / 8, Screen.width - Screen.width / 4, Screen.height / 4), "Evil Cubes", titlestyle);
        GUI.Label(new Rect(5, Screen.height - 45, Screen.width - 10, 40), "Game by Rao Zvorovski\nPlaytesting by Ervin Oro\nIn-game music by Kevin Macleod", attribution);
        if (menu == Menu.Mainmenu)
        {
            if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 2, Screen.width / 2, Screen.height / 8), "Play", buttonstyle))
                menu = Menu.Play;
            if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 2 + 5 + Screen.height / 8, Screen.width / 2, Screen.height / 8), "Statistics", buttonstyle))
                menu = Menu.Stats;
            if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 2 + 10 + Screen.height / 4, Screen.width / 2, Screen.height / 8), "Exit game", buttonstyle))
                Application.Quit();
        }
        else if (menu == Menu.Play)
        {
            if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 2, Screen.width / 2, Screen.height / 8), "Start new game", buttonstyle))
            {
                if (PlayerPrefs.HasKey("Version"))
                {
                    menu = Menu.SaveWillBeLost;
                }
                else
                {
                    GameWorld.Reset();
                    Application.LoadLevel("Play");
                }
            }
            if (PlayerPrefs.HasKey("Version"))
            {
                if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 2 + 5 + Screen.height / 8, Screen.width / 2, Screen.height / 8), "Resume game", buttonstyle))
                    Application.LoadLevel("Play");
            }
            if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 2 + 10 + Screen.height / 4, Screen.width / 2, Screen.height / 8), "Go back", buttonstyle))
            {
                menu = Menu.Mainmenu;
            }
        }
        else if (menu == Menu.SaveWillBeLost)
        {
            GUI.Label(new Rect(Screen.width / 4, Screen.height / 2, Screen.width / 2, Screen.height / 8), "Your save will be lost!\nAre you sure you want to continue?", warningstyle);
            if(GUI.Button(new Rect(Screen.width / 4, Screen.height / 2 + 5 + Screen.height / 8, Screen.width / 2, Screen.height / 8), "Yes", buttonstyle))
            {
                PlayerPrefs.DeleteKey("Version");
                GameWorld.Reset();
                Application.LoadLevel("Play");
            }
            if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 2 + 10 + Screen.height / 4, Screen.width / 2, Screen.height / 8), "No", buttonstyle))
            {
                menu = Menu.Play;
            }
        }
        else if (menu == Menu.Stats)
        {
            GUI.Box(new Rect(Screen.width / 4, Screen.height / 2, Screen.width / 2, Screen.height / 3), Globalstats.GetText(), statstyle);
            if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 2 + Screen.height / 3 + 5, Screen.width / 2, Screen.height / 6), "Back", buttonstyle))
                menu = Menu.Mainmenu;
        }
    }
}
