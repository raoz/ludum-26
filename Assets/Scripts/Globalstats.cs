using UnityEngine;
using System.Collections;

public static class Globalstats {
    public static int GetKills()
    {
        if (PlayerPrefs.HasKey("Kills"))
            return PlayerPrefs.GetInt("Kills");
        else
            return 0;
    }
    public static void AddKill()
    {
        if (PlayerPrefs.HasKey("Kills"))
            PlayerPrefs.SetInt("Kills", PlayerPrefs.GetInt("Kills") + 1);
        else
            PlayerPrefs.SetInt("Kills", 1);
    }
    public static int GetShreds()
    {
        if (PlayerPrefs.HasKey("Shreds"))
            return PlayerPrefs.GetInt("Shreds");
        else
            return 0;
    }
    public static void AddShred()
    {
        if (PlayerPrefs.HasKey("Shreds"))
            PlayerPrefs.SetInt("Shreds", PlayerPrefs.GetInt("Shreds") + 1);
        else
            PlayerPrefs.SetInt("Shreds", 1);
    }
    public static bool SetHighScore(int score)
    {
        if (PlayerPrefs.GetInt("Highscore", 0) < score)
        {            
            PlayerPrefs.SetInt("Highscore", score);
            return true;
        }
        return false;
    }
    public static string GetText()
    {
        return "You have killed " + GetKills() + " evil shapes\nYou have destroyed " + GetShreds() + " sheets of glass.\nYour high score is " + PlayerPrefs.GetInt("Highscore", 0) + ".";
    }
}
