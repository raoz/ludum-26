using UnityEngine;
using System.Collections;

public class Shooter2 : MonoBehaviour {
    public GameObject ammo;
    bool active = false;
    GameObject Player;
    float cooldown = 0;
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player)
            active = true;
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == Player)
            active = false;
    }

	void Update () {
        if (!active)
            return;
        float dist = Vector3.Distance(transform.position, Player.transform.position);        
        Vector3 dir = (Player.transform.position - transform.position).normalized;
        if(dist > 8)
            rigidbody.AddForce(dir * 100);
        if(dist < 5)
            rigidbody.AddForce(-dir * 100);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation((Player.transform.position - transform.position).normalized), Time.deltaTime * 5);
        if (cooldown <= 0)
        {
            RaycastHit hit;
            Physics.Raycast(new Ray(transform.position, dir), out hit, 15);
            if (hit.transform.tag == "Player")
            {
                Fire(dir);
                cooldown = .4f;
            }
            else
                cooldown = .3f;
        }
        else
        {
            cooldown -= Time.deltaTime;
        }
	}
    void Fire(Vector3 direction)
    {
        GameObject bullet = (GameObject)GameObject.Instantiate(ammo, transform.position + direction * 1.5f, Quaternion.identity);
        GetComponent<AudioSource>().Play();
        bullet.transform.name = "Bullet";
        bullet.rigidbody.velocity = (direction + Random.insideUnitSphere * 0.3f) * 30;
    }
    public void Death()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Health>().DoDamage(-25);
        Stats.Maxhealth += 20000F / (Stats.Maxhealth * Stats.Maxhealth);
    }
}
