using UnityEngine;
using System.Collections;

public class PassiveMelee : MonoBehaviour {
    int cnt = 0;
    void OnCollisionEnter(Collision hit)
    {
        Health health = hit.gameObject.GetComponent<Health>();
        if (health != null)
        {
            cnt++;
            if(cnt < 2)
                GetComponent<AudioSource>().Play();
        }
    }
    void OnCollisionStay(Collision hit)
    {
        Health health = hit.gameObject.GetComponent<Health>();
        if (health != null)
        {
            health.DoDamage(Time.deltaTime * 50);
        }
    }
    void OnCollisionExit(Collision hit)
    {
        Health health = hit.gameObject.GetComponent<Health>();
        if (health != null)
        {
            cnt--;
            if(cnt < 1)
                GetComponent<AudioSource>().Stop();
        }
    }
}
