using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public virtual void ChoosePosition(int x, int y)
    {
        transform.position = new Vector3(Random.Range(x + 1, x + GameWorld.TileSize.x), Random.Range(100, GameWorld.WorldSize.y * 100) / 100, Random.Range(y + 1, y + GameWorld.TileSize.y));
    }
    public void Death()
    {
        AudioSource.PlayClipAtPoint((AudioClip)Resources.Load("Sound/Scream"), transform.position, 1F);
        Stats.Maxhealth += 10000F / (Stats.Maxhealth * Stats.Maxhealth);
        Health health = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
        health.maxhealth = Stats.Maxhealth;
        health.DoDamage(-25);        
        Stats.Hitpower += 1f / (Stats.Hitpower * Stats.Hitpower * Stats.Hitpower);
        Globalstats.AddKill();
        Stats.KillCount++;
    }
}
