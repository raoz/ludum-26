using UnityEngine;
using System.Collections;

public class Charger : MonoBehaviour {
    bool active = false;
    GameObject Player;
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player)
            active = true;
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == Player)
            active = false;
    }
	// Update is called once per frame
	void Update () {
        if (!active)
            return;
        float dist = Vector3.Distance(transform.position, Player.transform.position);
        Vector3 dir = (Player.transform.position - transform.position).normalized;
        if (dist > 1)
            rigidbody.AddForce(dir * 2000);
	}
}
