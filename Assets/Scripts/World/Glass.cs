using UnityEngine;
using System.Collections;

public class Glass : MonoBehaviour {
    public float impact;
    public Vector3 origin  = Vector3.zero;
    public void Death()
    {
        Globalstats.AddShred();
        Stats.GlassCount++;
        transform.FindChild("Panel").gameObject.SetActive(false);
        Transform broken = transform.FindChild("Broken");
        broken.gameObject.SetActive(true);
        broken.parent = null;
        for (int i = 0; i < broken.GetChildCount(); i++)
        {
            broken.GetChild(i).rigidbody.AddForce((broken.GetChild(i).transform.position - origin) * impact);
        }
    }
}