using UnityEngine;
using System.Collections;

public class Shard : MonoBehaviour {

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.name == "Floor")
            Destroy(this);
    }
}
