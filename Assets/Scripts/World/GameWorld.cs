using UnityEngine;
using System.Collections;

public class GameWorld : MonoBehaviour
{
    public static readonly Vector3 WorldSize = new Vector3(100, 4, 100);
    public static readonly Vector2 TileSize = new Vector2(20, 20);
    public static Quaternion playerrotation = Quaternion.identity;
    public static int enemycount = 1;
    public GameObject[] enemies;
    public static float playerhealth = 100;
    float wallthickness = 0.5f;
    public GameObject Cube;
    public GameObject Lamp;
    public GameObject teleporter;
    public GameObject window;
    public static float timeScale = 1f;
    public static bool risetimescale = false;
    public static void Reset()
    {
        enemycount = 1;
        playerhealth = 100;
        playerrotation = Quaternion.identity;
        Stats.Cooldown = .5f;
        Stats.Hitpower = 2f;
        Stats.Maxhealth = 50;
        Health.GameOverPic = null;
        timeScale = 1f;
        risetimescale = false;
        Stats.KillCount = 0;
        Stats.GlassCount = 0;
    }
    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.HasKey("Version"))
        {
            Stats.Load();
        }
        CreateRooms();
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<Health>().SetHealth(playerhealth);
        player.transform.rotation = playerrotation;
    }
    void CreateRooms()
    {
        //Create floor
        GameObject floor = (GameObject)GameObject.Instantiate(Cube);
        floor.transform.localScale = new Vector3(WorldSize.x, wallthickness, WorldSize.z);
        floor.transform.name = "Floor";
        floor.transform.position = new Vector3(WorldSize.x / 2, -wallthickness / 2, WorldSize.z / 2);
        //Create ceiling
        GameObject ceil = (GameObject)GameObject.Instantiate(Cube);
        ceil.transform.localScale = new Vector3(WorldSize.x, wallthickness, WorldSize.z);
        ceil.transform.name = "Ceiling";
        ceil.transform.position = new Vector3(WorldSize.x / 2, wallthickness / 2 + WorldSize.y, WorldSize.z / 2);
        bool[,] connections = new bool[10, 6];
        int linkcount = 0;
        {
            int x = 7;
            int y = 4;
            linkcount++;
            Debug.Log(x + ":" + y);
            connections[x, y] = true;
            while (x > 1 || y > 0)
            {
                if ((Random.Range(0, 2) == 0 || y == 0) && x > 1)
                {
                    if (x % 2 == 0)
                        x--;
                    else
                        x -= 2;
                }
                else
                {
                    y--;
                    if (x % 2 == 1)
                        x--;
                }
                connections[x, y] = true;
                linkcount++;
                Debug.Log(x + ":" + y);
            }
        }
        Debug.Log(linkcount);
        int linkswanted = Random.Range(22, 30);
        Debug.Log(linkswanted);
        while (linkcount < linkswanted)
        {
            int x = Random.Range(0, 8);
            int y = Random.Range(0, 4);
            if (connections[x, y] == false)
            {
                linkcount++;
                connections[x, y] = true;
            }

        }

        //Create bounding walls
        for (int i = 0; i < 2; i++)
        {
            GameObject wall = (GameObject)GameObject.Instantiate(Cube);
            wall.transform.localScale = new Vector3(WorldSize.x * (i % 2) + wallthickness, WorldSize.y, WorldSize.z * ((i + 1) % 2) + wallthickness);
            wall.transform.name = "Wall" + i;
            Vector3 pos = new Vector3();
            switch (i)
            {
                case 0:
                    pos = new Vector3(0, WorldSize.y / 2, WorldSize.z / 2);
                    break;
                case 1:
                    pos = new Vector3(WorldSize.x / 2, WorldSize.y / 2, 0);
                    break;
                case 2:
                    pos = new Vector3(WorldSize.x, WorldSize.y / 2, WorldSize.z / 2);
                    break;
                case 3:
                    pos = new Vector3(WorldSize.x / 2, WorldSize.y / 2, WorldSize.z);
                    break;
                default:
                    pos = new Vector3(1000, 1000, 1000);
                    break;
            }
            wall.transform.position = pos;
        }
        //Create lamps and room separators
        GameObject lampparent = new GameObject();
        GameObject roomwalls = new GameObject();
        roomwalls.transform.name = "Room walls";
        lampparent.transform.name = "Lamps";
        for (float x = 0; x < WorldSize.x; x += TileSize.x)
        {
            for (float y = 0; y < WorldSize.z; y += TileSize.y)
            {
                if (x > 1 || y > 1)
                {
                    for (int i = 0; i < enemycount; i++)
                    {
                        DeployEnemy(x, y);
                    }
                }
                GameObject lamp = (GameObject)GameObject.Instantiate(Lamp);
                lamp.transform.position = new Vector3(x + TileSize.x / 2, WorldSize.y, y + TileSize.y / 2);
                lamp.transform.parent = lampparent.transform;
                lamp.SetActive(false);
                GameObject par = new GameObject();
                par.transform.parent = roomwalls.transform;
                par.transform.name = Mathf.RoundToInt(x * 2 / TileSize.x) + ":" + Mathf.RoundToInt(y / TileSize.y);
                bool firstopen = Random.Range(0, 2) != 0;
                if (connections[Mathf.RoundToInt(x * 2 / TileSize.x), Mathf.RoundToInt(y / TileSize.y)])
                {
                    GameObject wall1 = (GameObject)GameObject.Instantiate(Cube);
                    wall1.transform.localScale = new Vector3(TileSize.x * 0.4f + wallthickness, WorldSize.y, wallthickness);
                    wall1.transform.position = new Vector3(x + TileSize.x * 0.2f, WorldSize.y / 2, y + TileSize.y);
                    wall1.transform.parent = par.transform;
                    wall1 = (GameObject)GameObject.Instantiate(Cube);
                    wall1.transform.localScale = new Vector3(TileSize.x * 0.4f + wallthickness, WorldSize.y, wallthickness);
                    wall1.transform.position = new Vector3(x + TileSize.x * 0.8f, WorldSize.y / 2, y + TileSize.y);
                    wall1.transform.parent = par.transform;
                    wall1 = (GameObject)GameObject.Instantiate(Cube);
                    wall1.transform.localScale = new Vector3(TileSize.x * 0.15f + wallthickness, WorldSize.y * 0.2f, wallthickness);
                    wall1.transform.position = new Vector3(x + TileSize.x * 0.5f, WorldSize.y * 0.9f, y + TileSize.y);
                    wall1.transform.parent = par.transform;
                    GameObject win = (GameObject)GameObject.Instantiate(window, new Vector3(x + TileSize.x / 2, 1.5f, y + TileSize.y), Quaternion.identity);
                    win.name = "Glass";
                }
                else
                {
                    firstopen = false;
                    GameObject wall1 = (GameObject)GameObject.Instantiate(Cube);
                    wall1.transform.localScale = new Vector3(TileSize.x + wallthickness, WorldSize.y, wallthickness);
                    wall1.transform.position = new Vector3(x + TileSize.x * 0.5f, WorldSize.y / 2, y + TileSize.y);
                    wall1.transform.parent = par.transform;
                }
                if (connections[Mathf.RoundToInt((x * 2) / TileSize.x) + 1, Mathf.RoundToInt(y / TileSize.y)])
                {
                    GameObject wall2 = (GameObject)GameObject.Instantiate(Cube);
                    wall2.transform.localScale = new Vector3(wallthickness, WorldSize.y, TileSize.y * 0.4f + wallthickness);
                    wall2.transform.position = new Vector3(x + TileSize.x, WorldSize.y / 2, y + TileSize.y * 0.2f);
                    wall2.transform.parent = par.transform;
                    wall2 = (GameObject)GameObject.Instantiate(Cube);
                    wall2.transform.localScale = new Vector3(wallthickness, WorldSize.y, TileSize.y * 0.4f + wallthickness);
                    wall2.transform.position = new Vector3(x + TileSize.x, WorldSize.y / 2, y + TileSize.y * 0.8f);
                    wall2.transform.parent = par.transform;
                    wall2 = (GameObject)GameObject.Instantiate(Cube);
                    wall2.transform.localScale = new Vector3(wallthickness, WorldSize.y * 0.2f, TileSize.y * 1 + wallthickness);
                    wall2.transform.position = new Vector3(x + TileSize.x, WorldSize.y * 0.9f, y + TileSize.y * 0.5f);
                    wall2.transform.parent = par.transform;
                    GameObject win = (GameObject)GameObject.Instantiate(window, new Vector3(x + TileSize.x, 1.5f, y + TileSize.y / 2), Quaternion.LookRotation(new Vector3(90, 0, 0)));
                    win.name = "Glass";
                }
                else
                {

                    GameObject wall2 = (GameObject)GameObject.Instantiate(Cube);
                    wall2.transform.localScale = new Vector3(wallthickness, WorldSize.y, TileSize.y + wallthickness);
                    wall2.transform.position = new Vector3(x + TileSize.x, WorldSize.y / 2, y + TileSize.y * 0.5f);
                    wall2.transform.parent = par.transform;
                }
            }
        }
        //Deploy teleporter
        GameObject.Instantiate(teleporter, new Vector3(WorldSize.x - TileSize.x / 2, 0, WorldSize.z - TileSize.y / 2), Quaternion.identity);
    }
    void DeployEnemy(float x, float y)
    {
        GameObject enemy = (GameObject)GameObject.Instantiate(enemies[Random.Range(0, enemies.Length)]);
        enemy.GetComponent<Enemy>().ChoosePosition((int)x, (int)y);
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = timeScale;
        if (risetimescale)
        {
            timeScale += (Time.deltaTime / 4);
            if (timeScale > 1)
            {
                timeScale = 1;
                risetimescale = false;
            }
        }
    }
}
