using UnityEngine;
using System.Collections;

public class MouseManager : MonoBehaviour
{
    bool pause = false;
    public Texture crosshair;
    public GUIStyle style;
    void Awake()
    {
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pause = !pause;
        }
        if (!pause)
        {
            Screen.lockCursor = true;
            //#if !UNITY_STANDALONE_LINUX
            //            Screen.lockCursor = false;
            //#endif
            Screen.showCursor = false;
            Time.timeScale = GameWorld.timeScale;
            GameObject.FindGameObjectWithTag("Player").GetComponent<MouseLook>().enabled = true;
            Camera.main.gameObject.GetComponent<MouseLook>().enabled = true;
        }
        else
        {
            Screen.lockCursor = false;
            Screen.showCursor = true;
            Time.timeScale = 0;
            GameObject.FindGameObjectWithTag("Player").GetComponent<MouseLook>().enabled = false;
            Camera.main.gameObject.GetComponent<MouseLook>().enabled = false;
        }
        GameObject.FindGameObjectWithTag("Player").GetComponent<MouseLook>().sensitivityX = sensitivity * 1.2f;
        Camera.main.gameObject.GetComponent<MouseLook>().sensitivityY = sensitivity;
    }
    float sensitivity = 7;
    void OnGUI()
    {
        GUI.DrawTexture(new Rect(Screen.width / 2 - 7, Screen.height / 2 - 7, 15, 15), crosshair);

        if (pause)
        {
            GUI.Label(new Rect(Screen.width / 4, Screen.height / 4 , Screen.width / 2, Screen.height / 8), "PAUSED", style);
            GUI.Label(new Rect(Screen.width / 2, Screen.height - 20, Screen.width / 4 - 5, 20), "Mouse sensitivity");
            sensitivity = GUI.HorizontalScrollbar(new Rect(Screen.width - Screen.width / 4, Screen.height - 20, Screen.width / 4, 20), sensitivity, 0.1f, 0.5f, 25);               
        }
    }
    void OnDestroy()
    {
        Time.timeScale = 1;
        Screen.lockCursor = false;
        Screen.showCursor = true;
        pause = false;
    }
}