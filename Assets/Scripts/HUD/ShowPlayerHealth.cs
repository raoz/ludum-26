using UnityEngine;
using System.Collections;

public class ShowPlayerHealth : MonoBehaviour {
    public Health health;
    public Texture2D green;
    public Texture2D red;
    public GUIStyle style;
    float percentage;

    void Update()
    {
        percentage = (float)health.CurrentHealth / health.maxhealth;
    }
	// Update is called once per frame
	void OnGUI () {
        int greenlenght =  (int)(((Screen.width - 5) / 2) * percentage);
        GUI.DrawTexture(new Rect(5, 5, greenlenght, 20), green);
        GUI.DrawTexture(new Rect(5 + greenlenght, 5, (Screen.width - 5) / 2 - greenlenght, 20), red);
        GUI.Label(new Rect(5, 5, 100, 20), "Health: " + health.CurrentHealth.ToString("F2") + "/" + health.maxhealth.ToString("F2"), style);
	}
}
