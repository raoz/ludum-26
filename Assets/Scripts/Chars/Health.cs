using UnityEngine;
using System.Collections;
using System.Threading;

public class Health : MonoBehaviour
{
    public static Texture2D GameOverPic;
    bool matchange = false;
    float hurtdown = 0;
    float health = 100;
    public float maxhealth = 100;
    public float insphealth = 100;
    public Material hurtmaterial;
    Material normalmaterial;
    bool isplayer = false;
    bool gameover = false;

    void Start()
    {
        if (transform.tag == "Player")
        {
            isplayer = true;
            maxhealth = Stats.Maxhealth;
        }
        health = maxhealth;
        insphealth = health;
        if (renderer != null)
            normalmaterial = renderer.material;
    }

    public float CurrentHealth
    {
        get
        {
            return health;
        }
    }
    public void SetHealth(float health)
    {
        DoDamage(this.health - health);
    }

    public void DoDamage(float amount)
    {
        health -= amount;           
        if (renderer != null && amount > 1)
        {
            renderer.material = hurtmaterial;
            matchange = true;
        }
        if(isplayer && amount > 1)
            AudioSource.PlayClipAtPoint((AudioClip)Resources.Load("Sound/Playerhurt"), transform.position, 1);
        hurtdown = 0.2f;
        if (health <= 0)
        {
            Glass glass = GetComponent<Glass>();
            if (glass != null && glass.origin == Vector3.zero)
            {
                glass.origin = GameObject.FindGameObjectWithTag("Player").transform.position;
                glass.impact = Stats.Hitpower / 2;
            }
            if (isplayer)
            {
                gameover = true;
                //Application.Quit();
            }
            else
            {
                SendMessage("Death");
                Destroy(gameObject);
            }
        }
        if (health > maxhealth)
            health = maxhealth;
        if (health <= 0)
            health = 0;
        insphealth = health;
    }

    public void Update()
    {
        if (matchange)
        {
            if (hurtdown <= 0 && renderer != null)
            {
                renderer.material = normalmaterial;
                hurtdown = 2;
                matchange = false;
            }
            else
                hurtdown -= Time.deltaTime;
        }
        if (health < maxhealth)
            health += Time.deltaTime * 0.2f;
        if (isplayer)
        {
            GameWorld.playerhealth = health;
            maxhealth = Stats.Maxhealth;
            float calcdrag = ((maxhealth * maxhealth) / 5000);
            if (calcdrag < 1)
                calcdrag = 1;
            rigidbody.drag = calcdrag > 10 ? 10 : calcdrag;
            GetComponent<PhysicsController>().force = 4000 + (calcdrag - 1) * 4000;
            if (gameover)
            {
                StartCoroutine(GameOver());
                gameover = false;
            }
        }
    }
    IEnumerator GameOver()
    {
        Debug.Log("Game over");
        GameOverPic = new Texture2D(Screen.width, Screen.height);
        yield return new WaitForEndOfFrame();
        GameOverPic.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        yield return new WaitForFixedUpdate();
        GameOverPic.Apply();
        yield return new WaitForFixedUpdate();
        Application.LoadLevel("GameOver");
    }

    public void OnCollisionEnter(Collision hit)
    {
        float damage = (hit.impactForceSum.magnitude) / (hit.collider.bounds.size.magnitude * 5);
        Glass glass = gameObject.GetComponent<Glass>();
        if (glass != null && maxhealth - damage < 0)
        {
            glass.origin = hit.transform.position;
            glass.impact = hit.impactForceSum.magnitude * 2;
            hit.rigidbody.AddForce(-hit.impactForceSum);
        }
        DoDamage(damage);
        

    }
}
