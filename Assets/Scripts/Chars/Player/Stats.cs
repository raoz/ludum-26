using UnityEngine;
using System.Collections;

public static class Stats {

    public static float Hitpower = 2f;
    public static float Cooldown = .5f;
    public static float Maxhealth = 50;
    public static int KillCount = 0;
    public static int GlassCount = 0;
    public static void Save()
    {
        PlayerPrefs.SetInt("Version", 1);
        PlayerPrefs.SetInt("Level", GameWorld.enemycount);
        PlayerPrefs.SetFloat("MeleeDamage", Hitpower);
        PlayerPrefs.SetFloat("MeleeTime", Cooldown);
        PlayerPrefs.SetFloat("Maxhealth", Maxhealth);
        PlayerPrefs.SetFloat("Currenthealth", GameWorld.playerhealth);
        PlayerPrefs.SetInt("LocalKills", KillCount);
        PlayerPrefs.SetInt("Localshreds", GlassCount);
    }
    public static void Load()
    {

        GameWorld.enemycount = PlayerPrefs.GetInt("Level");
        Hitpower = PlayerPrefs.GetFloat("MeleeDamage");
        Cooldown = PlayerPrefs.GetFloat("MeleeTime");
        Maxhealth = PlayerPrefs.GetFloat("Maxhealth");
        GameWorld.playerhealth = PlayerPrefs.GetFloat("Currenthealth");
        KillCount = PlayerPrefs.GetInt("LocalKills");
        GlassCount = PlayerPrefs.GetInt("Localshreds");
    }
}
