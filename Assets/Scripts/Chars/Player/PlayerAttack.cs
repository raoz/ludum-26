using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {

    public float cooldown = 0;
    void Update()
    {
        if (cooldown <= 0)
        {
            if (Input.GetAxis("Attack") >= 0.99)
            {
                Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2));
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 10f))
                {
                    if (hit.transform.tag == "Hittable" && Vector3.Distance(transform.position, hit.transform.position) < 5)
                    {
                        AudioSource.PlayClipAtPoint((AudioClip)Resources.Load("Sound/Enemyhurt"), hit.transform.position, 1F);
                        Health health = hit.transform.GetComponent<Health>();
                        health.DoDamage(Stats.Hitpower);
                        Debug.Log("Hit, health left: " + health.CurrentHealth);
                        cooldown = Stats.Cooldown;
                    }
                }
            }
        }
        else
            cooldown -= Time.deltaTime;
    }
}
