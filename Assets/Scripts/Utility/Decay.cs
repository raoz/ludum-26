using UnityEngine;
using System.Collections;

public class Decay : MonoBehaviour {
    float starttime;
    public float time = 5;
    void Start()
    {
        time = time + Random.Range(-2, 3);
        starttime = Time.time;
    }
	void Update() {
        if (Time.time - starttime > time)
            Destroy(gameObject);
	}
}
