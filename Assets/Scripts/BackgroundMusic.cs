using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]
public class BackgroundMusic : MonoBehaviour
{
    public static BackgroundMusic backgroundmusic;
    public AudioClip peace;
    public AudioClip action;
    bool actionplaying = true;
    void Awake()
    {
        if (backgroundmusic == null)
            backgroundmusic = this;
        else
            Destroy(gameObject);
        DontDestroyOnLoad(this);
    }

    void Update()
    {
        if (Application.loadedLevel == 1)
            SetAction(true);
        else
            SetAction(false);
    }

    void SetAction(bool setting)
    {
        if (actionplaying == setting)
            return;
        actionplaying = setting;
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().clip = actionplaying ? action : peace;
        GetComponent<AudioSource>().Play();
    }
}
